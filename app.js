const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send("Welcome to company X application.");
});

module.exports = app;
